<?php

namespace app\logic\entities;

use app\logic\helpers\DateHelper;
use app\models\records\ProductAr;

/**
 * Class Product
 * @package app\logic\entities
 */
class Product extends ProductAr implements Entity
{
    /**
     * Product constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        // We ignore the fact that some products might exist already in our repository
        $this->created_at = DateHelper::nowSQL();
        $this->updated_at = DateHelper::nowSQL();
    }

    /**
     * @return bool
     */
    public function storeEnt()
    {
       return $this->save();
    }

    /**
     * @param int $productId
     * @return ProductAr|null
     */
    public function loadEnt($productId)
    {
        $this->product_id = $productId;
        $product = self::find()->one();
        return $product;
    }

}