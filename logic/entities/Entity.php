<?php

namespace app\logic\entities;

/**
 * Class Entity
 * @package app\logic\entities
 */
Interface Entity
{
    /** Implement the persistency layer.
     * Usually the connection with the framework is here.
     * @return bool
     */
    public function storeEnt();

    /**
     * @param mixed $entityId
     * @return Entity
     */
    public function loadEnt($entityId);
}