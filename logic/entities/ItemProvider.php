<?php

namespace app\logic\entities;

/**
 * Class ItemProvider
 * @package app\logic\entities
 */
abstract class ItemProvider implements VirtualEntity
{
    /** @var string $provider */
    protected $provider;

    /**
     * ItemProvider constructor.
     * @param string $provider
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
        return $this;
    }

    /**
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param string $provider
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param array $aObject
     * @return bool
     */
    abstract public function parseObjectIntoAttributes($aObject);
}
