<?php

namespace app\logic\entities\providers;


use app\logic\entities\ItemProvider;

/**
 * Class CapterraItem
 * @package app\logic\entities\providers
 */
class CapterraItem extends ItemProvider
{
    /** @var  string $name */
    protected $name;
    /** @var  string $twitter */
    protected $twitter;
    /** @var  string $tags */
    protected $tags;

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param array $aObject
     * @return bool
     */
    public function parseObjectIntoAttributes($aObject)
    {
        if ( array_key_exists('name', $aObject) ) {
            $this->name = $aObject['name'];
        } else {
            return false;
        }

        if ( array_key_exists('tags', $aObject) ) {
            $this->tags = $aObject['tags'];
        }

        if ( array_key_exists('twitter', $aObject) ) {
            $this->twitter = $aObject['twitter'];
        }

        return true;
    }
}