<?php

namespace app\logic\entities\providers;

use app\logic\entities\ItemProvider;

/**
 * Class SoftwareAdviceItem
 * @package app\logic\entities\providers
 */
class SoftwareAdviceItem extends ItemProvider
{
    /** @var  string $title */
    protected $title;
    /** @var  string $twitter */
    protected $twitter;
    /** @var  string $categories */
    protected $categories;

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * @return string
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param array $aObject
     * @return bool
     */
    public function parseObjectIntoAttributes($aObject)
    {
        if ( array_key_exists('title', $aObject) ) {
            $this->title = $aObject['title'];
        } else {
            return false;
        }

        if ( array_key_exists('categories', $aObject) ) {
            $this->categories = implode(', ', $aObject['categories']);
        }

        if ( array_key_exists('twitter', $aObject) ) {
            $this->twitter = $aObject['twitter'];
        }

        return true;
    }
}
