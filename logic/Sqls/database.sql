

CREATE DATABASE `saas_products` /*!40100 DEFAULT CHARACTER SET utf8 */;


CREATE TABLE `products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `provider_name` varchar(100) NOT NULL COMMENT 'Possible FK to table providers, should be provider Id',
  `name` varchar(150) NOT NULL COMMENT 'Name or title of the software',
  `twitter_account` varchar(150) DEFAULT NULL COMMENT 'Twitter account or name',
  `software_categories` varchar(255) DEFAULT NULL COMMENT 'Tags or categories. Usually we would create another table, one to many.',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='To store all products from providers';
