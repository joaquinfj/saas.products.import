<?php

namespace app\logic\helpers;

/**
 * Class FeedHelper
 * @package app\logic\helpers
 */
class FeedHelper
{
    const PATH = 'PATH';
    const URL = 'URL';
    const API = 'API';
    /** @var string $fileType */
    protected $fileType;
    /** @var  string $file */
    protected $file;

    /**
     * @param mixed $source
     * @return bool
     * @throws \Exception
     */
    public function parseSource($source)
    {
        if (is_string($source) && file_exists($source)) {
            $this->fileType = FeedHelper::PATH;
        } elseif (is_string($source) && filter_var($source, FILTER_VALIDATE_URL) !== false) {
            $this->fileType = FeedHelper::URL;
        } else {
            $this->fileType = FeedHelper::API;
        }

        switch($this->fileType) {
            case self::PATH:
                $this->file = file_get_contents($source);
                break;
            case self::URL:
                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch, CURLOPT_URL, $source);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $data = curl_exec($ch);
                curl_close($ch);
                $this->file = $data;
                break;
            default:
                throw new \Exception('Not implemented', 501);
                break;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
}