<?php

namespace app\logic\helpers;

use \DateTime;

/**
 * Class DateHelper
 * @package app\logic\helpers
 */
class DateHelper
{
    const SEPARATOR = "-";
    const SEPARATOR_TIME = ":";

    // Time units and types aliases in seconds. Used for different purposes
    const MILLISECONDS = 0.001;
    const SECS = 1;
    const MINUTES = 60;
    const HOURS = 3600;
    const DAY_SECS = 86400;
    const WEEK_SECS = 604800;

    const MYSQL = 'MYSQL';
    const PHP = 'PHP';
    const EN_STRING = 'EN-STRING';

    const SUNDAY = 'SUNDAY';
    const MONDAY = 'MONDAY';
    const TUESDAY = 'TUESDAY';
    const WEDNESDAY = 'WEDNESDAY';
    const THURSDAY = 'THURSDAY';
    const FRIDAY = 'FRIDAY';
    const SATURDAY = 'SATURDAY';
    const WEEKDAYS = 'WEEKDAYS';
    const WEEKEND = 'WEEKEND';
    const ALL_WEEK = 'ALL-WEEK';

    const WEEK_MINUTES = 10080;

    /**
     * Checks if the date passed matches the format YYYY-MM-DD
     * @static
     *
     * @param string $dateYMDHIS
     * @param bool $withTime
     *
     * @return bool
     */
    public static function validDateTimeSQL($dateYMDHIS, $withTime = false)
    {
        if (!is_string($dateYMDHIS) || (!$withTime && strlen($dateYMDHIS)!=10) ||
            ($withTime && strlen($dateYMDHIS) != 19)) {
            return false;
        }

        if ($withTime) {
            if (!self::validTimeSQL(substr($dateYMDHIS, 11, 8))) {
                return false;
            }

            $dateYMDHIS = substr($dateYMDHIS, 0, 10);
        }

        return self::validDateSQL($dateYMDHIS);
    }

    /**
     * @param string $dateYMD
     * @return bool
     */
    public static function validDateSQL($dateYMD)
    {
        $year = explode(self::SEPARATOR, $dateYMD);
        $month = (int)$year[1];
        $day = (int)$year[2];
        $year = (int)$year[0];
        return $day >= 1 && $day <= 31 && $month >= 1 && $month <= 12 && $year >= 1900 && $year <= 2900;
    }

    /**
     * @param string $timeHHMMSS
     * @return bool
     */
    public static function validTimeSQL($timeHHMMSS)
    {
        $aTime = explode(self::SEPARATOR_TIME, $timeHHMMSS);
        $hour = (int)$aTime[0];
        $minutes = (int)$aTime[1];
        $seconds = (int)$aTime[2];
        if ($hour < 0 || $hour > 24 || $minutes < 0 || $minutes > 59 || $seconds < 0 || $seconds > 59) {
            return false;
        }

        return true;
    }

    /**
     * @static
     *
     * @param        $sqlDate
     * @param string $separatorIN
     *
     * @return string
     */
    public static function sqlToEuropean($sqlDate, $separatorIN = '-')
    {
        $all = explode($separatorIN, $sqlDate);
        $dd = intval($all[2]);
        $mm = intval($all[1]);
        $year = intval($all[0]);

        $sqlDate = $dd . $separatorIN . $mm . $separatorIN . $year;

        return $sqlDate;
    }

    /** Returns a DateTime object
     * @param string $date format dd-mm-yyyy is expected
     * @return bool|\DateTime
     */
    public static function europeanToUnixTime($date)
    {
        $all = explode(self::SEPARATOR, $date);
        $yyyy = intval($all[2]);
        $month = intval($all[1]);
        $day = intval($all[0]);

        $sqlDate = $yyyy.self::SEPARATOR.str_pad($month, 2, '00', STR_PAD_LEFT).
            self::SEPARATOR.str_pad($day, 2, '00', STR_PAD_LEFT);

        if (!self::validDateTimeSQL($sqlDate)) {
            return false;
        }

        return self::sqlToDateTime($sqlDate);
    }


    /**
     * Returns the time UTC as build by the string (i.e. -7 day)
     *
     * @param string $dateString
     * @return \DateTime
     */
    public static function now($dateString = 'now')
    {
        $dateTimeNow = new \DateTime($dateString, new \DateTimeZone(\Yii::$app->getTimeZone()));
        return $dateTimeNow;
    }

    /** Returns the current time for the time zone of the app
     * @param bool $withMicroSeconds
     * @return string
     */
    public static function nowSQL($withMicroSeconds = false)
    {
        $dateTimeNow = self::now();
        if (!$withMicroSeconds) {
            return $dateTimeNow->format('Y-m-d H:i:s');
        } else {
            $microtime = self::unixTimestamp(true);
            $mSecs = $microtime - floor($microtime);
            //$mSecs = substr($mSecs, 1);

            return $dateTimeNow->format('Y-m-d H:i:s') . substr(strval(round($mSecs, 6)), 1);
        }
    }

    /**
     * @param bool $withMicrosecs
     * @return int|float
     */
    public static function unixTimestamp($withMicrosecs = false)
    {
        if ($withMicrosecs) {
            return microtime(true);
        } else {
            $dateTimeNow = self::now();
            return $dateTimeNow->getTimestamp();
        }
    }

    /**
     * @param string $sqlDate format YYYY-MM-DD is expected
     * @param bool $withTime
     * @return bool|\DateTime
     */
    public static function sqlToDateTime($sqlDate, $withTime = false)
    {
        if (!self::validDateTimeSQL($sqlDate, $withTime)) {
            return false;
        }

        $dtime = new \DateTime($sqlDate);

        return $dtime;
    }

    /** Returns the current time for the time zone of the app
     *
     * @param \DateTime $dateTime
     * @param bool|false $withTime
     * @return string
     */
    public static function dateTimeToSql($dateTime, $withTime = false)
    {
        if ($withTime) {
            return $dateTime->format('Y-m-d H:i:s');
        }

        return $dateTime->format('Y-m-d');
    }

    /**
     * Converts DateTime to Unix time
     *
     * @param \DateTime $dateTime
     * @return int
     */
    public static function dateTimeToUnixtime(\DateTime $dateTime)
    {
        return intval($dateTime->format("U"));
    }

    /**
     * @param string $sqlDate format YYYY-MM-DD is expected
     * @param bool $withTime
     * @return bool|\DateTime
     */
    public static function sqlToUnixTime($sqlDate, $withTime = false)
    {
        if (!self::validDateTimeSQL($sqlDate, $withTime)) {
            return false;
        }

        $dtime = new \DateTime($sqlDate);
        return $dtime->format("U");
    }

    /**
     * @param integer $unixTime
     *
     * @return bool|\DateTime
     */
    public static function unixTimeToDateTime($unixTime)
    {
        if (!is_numeric($unixTime)) {
            return false;
        }
        $dtime = new \DateTime();
        $dtime->setTimestamp(intval($unixTime));
        return $dtime;
    }

    /**
     * @param int $unixTime
     * @return bool|string
     */
    public static function unixTimeToSql($unixTime)
    {
        if (!is_numeric($unixTime)) {
            return false;
        }

        $dTime = self::unixTimeToDateTime($unixTime);
        return self::dateTimeToSql($dTime, true);
    }

    /**
     * @param \DateTime $dateTime
     * @return \DateTime
     */
    public static function getDateLastDayOfMonth(\DateTime $dateTime)
    {
        return new \DateTime($dateTime->format('Y-m-t'));
    }

    /**
     * @param \DateTime $dateTime
     * @param int $minutes
     * @return \DateTime
     */
    public static function addRemoveMinutesToDateTime(\DateTime $dateTime, $minutes)
    {
        $newTime = clone($dateTime);
        $sign = ($minutes<0)?'':'+';
        $dtChanged = $newTime->modify($sign . strval($minutes) . ' minutes');
        return $dtChanged;
    }

    /**
     * @param DateTime $dateTime
     * @param $stringInterval
     * @return bool|DateTime
     */
    public static function addIntervalToDateTime(\DateTime $dateTime, $stringInterval)
    {
        $dateTimeAdded = clone($dateTime);
        try {
            if (substr($stringInterval, 0, 1) == '-') {
                $stringInterval = substr($stringInterval, 1);
                $interval = new \DateInterval($stringInterval);
                $interval->invert = 1;
            } else {
                $interval = new \DateInterval($stringInterval);
            }
        } catch (\Exception $e) {
            return false;
        }

        return $dateTimeAdded->add($interval);
    }

    /**
     * @example
     *  $dateTime1 = 2016-04-12 13:00:00
     *  $dateTime2 = 2016-04-12 13:30:00
     *    Difference in hours (25)= 0.5
     *    Difference in minutes = 30
     *
     * @param int|\DateTime $dateTime1
     * @param int|\DateTime $dateTime2
     * @param int $format
     * @return float
     */
    public static function getDiffdateTime($dateTime1, $dateTime2, $format = self::MINUTES)
    {
        if ((is_float($dateTime1) && is_float($dateTime2)) || (is_int($dateTime1) && is_int($dateTime2))) {
            $milliseconds = $dateTime2 - $dateTime1;
            if ($format == self::MILLISECONDS) {
                return $milliseconds;
            }
            $seconds = $milliseconds * 1000;
        } else {
            $sqlTime1 = strtotime(self::dateTimeToSql($dateTime1, true));
            $sqlTime2 = strtotime(self::dateTimeToSql($dateTime2, true));
            $seconds = $sqlTime2 - $sqlTime1;
        }

        return $seconds / $format;
    }

    /**
     * @param DateTime $dateTime
     * @param string $seconds
     * @return DateTime
     */
    public static function setSeconds(DateTime $dateTime, $seconds = '00')
    {
        $newTime = $dateTime->setTime(intval($dateTime->format('H')), intval($dateTime->format('i')), intval($seconds));
        return $newTime;
    }

    /**
     * @param \DateTime|int $dateTime; Could be PHP format or DateTime
     * @param string $format; could be EN-STRING, MYSQL(1-Su to 7-Sat), PHP (0-Su to 6-Sa)
     * @return mixed
     */
    public static function getDayOfWeek($dateTime, $format = 'EN-STRING')
    {
        $dayOfWeekPHP = $dateTime;
        if (!is_int($dateTime)) {
            $dayOfWeekPHP = intval($dateTime->format('w'));
        }

        if ($format == 'PHP') {
            return $dayOfWeekPHP;
        } elseif ($format == 'MYSQL') {
            return $dayOfWeekPHP+1;
        } else {
            switch ($dayOfWeekPHP) {
                case 0:
                    return self::SUNDAY;
                    break;
                case 1:
                    return self::MONDAY;
                    break;
                case 2:
                    return self::TUESDAY;
                    break;
                case 3:
                    return self::WEDNESDAY;
                    break;
                case 4:
                    return self::THURSDAY;
                    break;
                case 5:
                    return self::FRIDAY;
                    break;
                case 6:
                    return self::SATURDAY;
                    break;
                default:
                    return false;
                    break;
            }
        }
    }

    /**
     * @return array
     */
    public static function getTypeOfDays()
    {
        return [
            self::SUNDAY => 'SUNDAY',
            self::MONDAY => 'MONDAY',
            self::TUESDAY => 'TUESDAY',
            self::WEDNESDAY => 'WEDNESDAY',
            self::THURSDAY => 'THURSDAY',
            self::FRIDAY => 'FRIDAY',
            self::SATURDAY => 'SATURDAY',
            self::WEEKDAYS => 'WEEKDAYS',
            self::WEEKEND => 'WEEKEND',
            self::ALL_WEEK => 'ALL-WEEK'
        ];
    }

    /**
     * @param DateTime $dateTime
     * @param string $format
     * @return string
     */
    public static function getTimeOnly(\DateTime $dateTime, $format = 'H:i:s')
    {
        return $dateTime->format($format);
    }

    /**
     * @param string $dayOfWeek
     * @param string $timeHIS
     * @return bool|DateTime
     */
    public static function getFakeDateBasedDayWeek($dayOfWeek, $timeHIS)
    {
        // Random week to control change of daysRange : 23/10/2016(SUNDAY) -->> 29/10/2016(SATURDAY)
        switch ($dayOfWeek) {
            case self::SUNDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-23 '.$timeHIS, true);
                break;
            case self::MONDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-24 '.$timeHIS, true);
                break;
            case self::TUESDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-25 '.$timeHIS, true);
                break;
            case self::WEDNESDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-26 '.$timeHIS, true);
                break;
            case self::THURSDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-27 '.$timeHIS, true);
                break;
            case self::FRIDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-28 '.$timeHIS, true);
                break;
            case self::SATURDAY:
                $dateTimeFake = self::sqlToDateTime('2016-10-29 '.$timeHIS, true);
                break;
            default:
                $dateTimeFake = self::sqlToDateTime('2016-10-23 '.$timeHIS, true);
                break;
        }

        return $dateTimeFake;
    }
}
