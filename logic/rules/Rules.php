<?php

namespace app\logic\rules;

use yii\di\Container;
use yii\helpers\Console;
use yii\log\Logger;

use app\components\RulesCustomException;

/**
 * Class Rules
 * @package app\logic\rules
 */
abstract class Rules
{
    /** @var bool $useExceptionHandling */
    public static $useExceptionHandling = false;
    /** @var bool $usePersistence */
    public static $usePersistence = false;

    const ERR_NOT_IMPLEMENTED = 501;
    const ERR_NOT_ALLOWED = 403;
    const ERR_NOT_EXPECTED = 502;

    const SUCCESS = true;

    /** @var  Container $container */
    protected $container;
    /** @var integer $errorCode */
    protected $errorCode;
    /** @var  string $errorMessage */
    protected $errorMessage;
    /** @var  Console|Logger $logger */
    protected $logger;

    /**
     * Rules constructor.
     * @param Container $container
     * @param Console|Logger $logger
     */
    public function __construct(Container $container, $logger)
    {
        $this->container = $container;
        $this->setLogger($logger);

        return $this;
    }

    /**
     * @param string $errorMessage
     * @param int $errorCode
     * @param bool $forceThrowException To force throw new Exception
     *
     * @throws RulesCustomException
     * @return bool
     */
    protected function returnError($errorMessage, $errorCode = 0, $forceThrowException = false)
    {
        if (empty($errorCode)) {
            $errorCode = RulesCustomException::CODE_GENERIC;
        }

        $this->errorCode = $errorCode;
        if ($this->errorMessage=='') {
            $this->errorMessage = $errorMessage;
        } else {
            $this->errorMessage .= " \n " . $errorMessage;
        }

        if (self::$useExceptionHandling || $forceThrowException) {
            throw new RulesCustomException($errorMessage, $errorCode);
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $msg
     */
    public function setErrorMessage($msg)
    {
        $this->errorMessage = $msg;
    }

    /**
     * @param Console|Logger $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $message
     */
    protected function logError($message)
    {
        $logger = $this->logger;
        if ($logger instanceof Logger) {
            $logger->log($message, $logger::LEVEL_ERROR, 'Rules');
        } elseif ($logger instanceof Console) {
            $logger->stderr($message);
        } else {
            echo $message;
        }
    }

    /**
     * @param string $message
     */
    protected function logInfo($message)
    {
        $logger = $this->logger;
        if ($logger instanceof Logger) {
            $logger->log($message, $logger::LEVEL_INFO, 'Rules');
        } elseif ($logger instanceof Console) {
            $logger->stdout($message . " \n");
        } else {
            echo $message;
        }
    }
}
