<?php

namespace app\logic\rules\providers;

use app\logic\entities\ItemProvider;
use app\logic\entities\Product;
use app\logic\entities\providers\SoftwareAdviceItem;

/**
 * Class SoftwareAdviceProvRu
 * @package app\logic\rules\providers
 */
class SoftwareAdviceProvRu extends ProvidersRu
{
    public static $providerName = 'Software-advice';
    /**
     *
     * @param string $source
     * @return ItemProvider[]|bool
     */
    public function parseSourceIntoItems($source)
    {
        $jsonObjects = json_decode($source, true);
        $jsonProds = $jsonObjects['products'];

        $items = array();
        foreach ($jsonProds as $jsonObject ) {
            /** @var SoftwareAdviceItem $softAdviceItem */
            $softAdviceItem = $this->container->get('Entities\SoftwareAdviceItem', [self::$providerName]);
            if (!$softAdviceItem->parseObjectIntoAttributes($jsonObject)) {
                // If we want to skip error lines or not, and so continue, we decide it here among other places.
                return $this->returnError('Item '. 'Software-Advice' .' in feed could not be parsed.' .
                    'We abort the rest of the import');
            }

            $items[] = $softAdviceItem;
        }

        return $items;
    }


    /**
     * @param SoftwareAdviceItem|ItemProvider $item
     * @return Product
     */
    public function mapItemToProduct($item)
    {
        /** @var Product $product */
        $product = $this->container->get('Entities\Product');
        $product->name = $item->getTitle();
        $product->provider_name = $item->getProvider();
        $product->software_categories = $item->getCategories();
        $product->twitter_account = $item->getTwitter();

        return $product;
    }
}