<?php

namespace app\logic\rules\providers;

use app\logic\entities\ItemProvider;
use app\logic\entities\Product;
use app\logic\entities\providers\CapterraItem;

/**
 * Class CapterraProvRu
 * @package app\logic\rules\providers
 */
class CapterraProvRu extends ProvidersRu
{
    public static $providerName = 'Capterra';

    /**
     * tags: "Bugs & Issue Tracking,Development Tools"
     * name: "GitGHub"
     * twitter: "github"
     *
     * @param string $source
     * @return ItemProvider[]|bool
     */
    public function parseSourceIntoItems($source)
    {
        $ymlObjects = yaml_parse($source);
        $items = array();
        foreach ( $ymlObjects as $ymlObject ) {
            /** @var CapterraItem $capterraItem */
            $capterraItem = $this->container->get('Entities\CapterraItem', [self::$providerName]);
            if (!$capterraItem->parseObjectIntoAttributes($ymlObject)) {
                // If we want to skip error lines or not, and so continue, we decide it here among other places.
                return $this->returnError('Item '. $ymlObject['name'] .' in feed could not be parsed.' .
                    'We abort the rest of the import');
            }

            $items[] = $capterraItem;
        }

        return $items;
    }


    /**
     * @param CapterraItem|ItemProvider $item
     * @return Product
     */
    public function mapItemToProduct($item)
    {
        /** @var Product $product */
        $product = $this->container->get('Entities\Product');
        $product->name = $item->getName();
        $product->provider_name = $item->getProvider();
        $product->software_categories = $item->getTags();
        $product->twitter_account = $item->getTwitter();

        return $product;
    }
}