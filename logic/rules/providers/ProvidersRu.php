<?php

namespace app\logic\rules\providers;

use app\logic\entities\ItemProvider;
use app\logic\entities\Product;
use app\logic\rules\Rules;

/**
 * Class ProvidersRu
 * @package app\logic\rules\providers
 */
abstract class ProvidersRu extends Rules
{
    public static $providerName;
    /**
     * @param string $source
     * @return Product[]|bool
     */
    public function readFeed($source)
    {
        $items = $this->parseSourceIntoItems($source);
        $countItems = count($items);
        if ($countItems<=0) {
            return $this->returnError('No items found in the provider\'s feed. Please check content of source.');
        }

        $products = $this->mapItemsToProducts($items);
        $countProds = count($products);
        if ($countItems!==$countProds) {
            return $this->returnError('There were ' . $countItems . ' items in the provider feed, '.
                'however only '.$countProds . ' were correctly parsed into proper products. '.
                'Please double check before proceed.');
        }

        return $products;
    }

    /**
     * @param string $source
     * @return ItemProvider[]
     */
    abstract public function parseSourceIntoItems($source);

    /**
     * @param ItemProvider[] $items
     * @return Product[]
     */
    protected function mapItemsToProducts($items)
    {
        $products = array();
        foreach ($items as $item) {
            $products[] = $this->mapItemToProduct($item);
        }

        return $products;
    }

    /**
     * @param ItemProvider $item
     * @return Product
     */
    abstract public function mapItemToProduct($item);
}