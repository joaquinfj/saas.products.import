<?php

namespace app\logic\rules\products;

use app\logic\rules\Rules;
use app\logic\entities\Product;
use app\logic\rules\providers\ProvidersRu;

/**
 * Class ProductsRu
 * @package app\logic\rules\products
 */
class ProductsRu extends Rules
{
    /** In case we process thousands things should be approached
     * in a more scalabe manners, work in batches. */
    const LIMIT_TO_BATCH = 10000;

    /** @var  ProvidersRu $providerRu */
    protected $providerRu;

    /**
     * @param string $providerName
     * @param string $source
     * @return bool
     */
    public function importFromProvider($providerName, $source)
    {
        try {
            $this->providerRu = $this->container->get('Rules\\' . ucwords($providerName) . 'ProvRu',
                [$this->container, $this->logger]);
        } catch (\Exception $exception) {
            return $this->returnError('The products import for provider ' . $providerName .
                ' has not been yet implemented or the name of provider is wrong.' .
                ' Try captial letters for words: ' . $exception->getMessage(),
                self::ERR_NOT_IMPLEMENTED);
        }

        /** @var Product[] $products */
        $products = $this->providerRu->readFeed($source);
        if ($products===false) {
            return $this->returnError('Products from provider' . $providerName .
                ' could not be correctly imported. Possible cause: ' . $this->providerRu->getErrorMessage(),
                self::ERR_NOT_EXPECTED);
        } elseif ( count($products)===0 ) {
            return self::SUCCESS;
        }

        return $this->saveAll($products);
    }

    /**
     * @param Product[] $products
     * @return bool
     */
    public function saveAll(array $products)
    {
        if(count($products)>=self::LIMIT_TO_BATCH) {
            // ... We use different method to store thousands.
        }

        $allSuccess = true;
        foreach ($products as $product) {
            if (!$this->save($product)) {
                $this->logError('... Error storing: Name: ' . $product->name );
                $allSuccess = false;
            } else {
                $this->logInfo('... Success importing: Name: ' . $product->name .
                    ', Categories: '. $product->software_categories .'; Twitter: '. $product->twitter_account . ';');
            }
        }

        return $allSuccess;
    }

    /**
     * @param Product $product
     * @return bool
     */
    protected function save(Product $product)
    {
        if (self::$usePersistence) {
            return $product->storeEnt();
        }

        return true;
    }

}
