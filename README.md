Saas Product import test by Joaquim Forcada Jiménez
============================

 - Language: PHP 5.4
 - **Yii 2 Basic Project Template , Yii2 Framework**. [Yii 2](http://www.yiiframework.com/)
 - All logic and implementation has been done trying to 
 lay above the framework itself, so is quite independent from the framework itself
 - **Optional**: Mysql as database persistency repository.


Introduction
============================
This excercise was implemented thinking that the number of providers, feed locations and source types is going 
to increase, evolve and change and that several developers are going to work with it. Also to make it "easy reading" and 
"intuitive".
There hundreds of other ways to do it, and to make it much more dynamic, without the need to even write the whole definition 
 properties and config of providers feeds. 
 **We could set everything in parameters within a database or configuration files, so no source code at all has to be modified at all.**
If I had more time, I would set all the mapping between our "products catalog" features and "providers catalog" features 
all in a database table. I guessed this was not the goal of the exercise.

Also, just for your info, **all is fully working, even with Database persistency layer**, regardless of which Database Engine.

DIRECTORY STRUCTURE ABOVE YII-2 (PHP Framework)
-------------------
  - This layer of code is above the Yii2 framework and is independent from it. **It is kind of DDD design**.
  - There are some "points of connection" and that's it. 
  The connections are between Entities and "Active Records from Yii2"
  , and Rules and "Active Queries from Yii2"
  - This **directory "logic/"** contains all the logic for importing multiple product feeds 
  from across several providers and sources types and locations:
    
~~~
    logic/              Contains all logic and classes relevant for the whole test
    logic/rules/        Business rules, end real-life operations should be implemented here
    logic/helpers/      Standard tools, technical services, communications, etc.
    logic/entities/     Entities or objects like provider, product, line in the feed, categories, etc.
    logic/sqls/         Sql to help make this example work. Simply all sqls necessary
~~~


Yii2 DIRECTORY STRUCTURE
-------------------
  This is the general folder structure within a Yii2 framework project:
    
      assets/             Yii2-contains assets definition
      commands/           Yii2-contains console commands (controllers)
      config/             Yii2-contains application configurations
      controllers/        Yii2-contains Web controller classes
      mail/               Yii2-contains view files for e-mails
      models/records      Yii2-contains model classes, **Database layer**; can be Mongo, Mysql, etc.
      models/queries      Yii2-contains model classes, **Database layer**
      runtime/            Yii2-contains files generated during runtime
      tests/              Yii2-contains various tests for the basic application
      vendor/             Yii2-contains dependent 3rd-party packages
      views/              Yii2-contains view files for the Web application
      web/                Yii2-contains the entry script and Web resources

INSTALLATION
------------
### A. REQUIREMENTS

 * The minimum requirement by this project is PHP 5.4.0.
 * Install **YAML extension for PHP**,quite annoying (For Capterra feed):
     - Download and install YAML: http://pyyaml.org/wiki/LibYAML
     - Then: >>pecl install yaml
 
### B. Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

* You will need some features of composer installed globally:
~~~
php composer.phar global require "fxp/composer-asset-plugin:^1.2.0"
~~~


CONFIGURATION
-------------

### Database
OPTIONAL: 
If you want to see it fully working, please enable persistency changing to **TRUE**:
~~~
 \app\logic\rules\Rules::$usePersistence
~~~
You have the database SQL schema under:
~~~
/var/www/saas.product.joaquim/logic/Sqls/database.sql
~~~
Edit the file `config/db.php` to connect to your server:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=saas_products',
    'username' => 'root',
    'password' => '123456789',
    'charset' => 'utf8',
];
```
RUNNING
-------
Please just **run from your command line**:
~~~
php yii import Capterra feed-products/capterra.yaml
~~~
~~~
joaquim@joaquim-ubuntu:/var/www/saas.product.joaquim$  
**** 2016-11-03 18:19:05.992038 START import on  Capterra from feed-products/capterra.yaml 
 ... Success importing: Name: GitGHub, Categories: Bugs & Issue Tracking,Development Tools; Twitter: github; 
 ... Success importing: Name: Slack, Categories: Instant Messaging & Chat,Web Collaboration,Productivity; Twitter: slackhq; 
 ... Success importing: Name: JIRA Software, Categories: Project Management,Project Collaboration,Development Tools; Twitter: jira; 
2016-11-03 18:19:06.017924*** END successful import on Capterra from feed-products/capterra.yaml 
~~~
And for the other providers...the same:
~~~ 
php yii import SoftwareAdvice feed-products/softwareadvice.json
~~~
~~~
joaquim@joaquim-ubuntu:/var/www/saas.product.joaquim$ php yii import SoftwareAdvice feed-products/softwareadvice.json 
**** 2016-11-03 18:19:26.07186 START import on  SoftwareAdvice from feed-products/softwareadvice.json 
 ... Success importing: Name: Freshdesk, Categories: Customer Service, Call Center; Twitter: @freshdesk; 
... Success importing: Name: Zoho, Categories: CRM, Sales Management; Twitter: ; 
2016-11-03 18:19:26.096076*** END successful import on SoftwareAdvice from feed-products/softwareadvice.json 

~~~


UNIT TESTING
-------
Testing is incomplete. **I only implemented 2 tests**. 

With more time probably I would have implemented 
**all tests for logic/** directory.

Tests are located in `tests` directory. They are developed with PHP Unit, instead of default 
PHP Code Deception:
    
      tests/unit/             Unit tests, tradtional PHP Unit

Tests can be executed by running:
```
php --bootstrap bootstrap.php --configuration phpunit.xml tests\unit\logic\rules\providers\SoftwareAdviceProvRuTest 
``` 

The command above will execute unit tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. **Due to the fact that last 1 year I am almost not implementing tests nor running them 
I admit tests could be much better.**

FUNCTIONAL TESTING
-------
There is none. 
The reason is that usually **for me functional testing is done for API calls and end browser usage**. 
In this example we do not have either of them.  
However let me tell you I have in the past implemented many Selenium tests across 3 browsers to test the main flows of web sites.
I only have used Selenium + PHP Unit.
