<?php

namespace app\components;

use yii\base\Exception;

/** Exception handler for Rules execution
 * Class RulesCustomException
 * @package components
 */
class RulesCustomException extends Exception
{
    const CODE_GENERIC = 500;

    /**
     * Constructor.
     * @param string $message error message
     * @param integer $code error code
     * @param \Exception $previous The previous exception used for the exception chaining.
     */
    public function __construct($message = null, $code = 0, \Exception $previous = null)
    {
        parent::__construct('Exception within Business Rules Logic: '.$message, $code, $previous);
    }
}