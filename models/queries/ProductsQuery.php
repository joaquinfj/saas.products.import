<?php

namespace app\models\queries;

/**
 * This is the ActiveQuery class for [[\app\models\records\ProductAr]].
 *
 * @see \app\models\records\ProductAr
 */
class ProductsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\records\ProductAr[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\records\ProductAr|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
