<?php

namespace app\models\records;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $product_id
 * @property string $provider_name
 * @property string $name
 * @property string $twitter_account
 * @property string $software_categories
 * @property string $updated_at
 * @property string $created_at
 */
class ProductAr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider_name', 'name'], 'required'],
            [['updated_at', 'created_at', 'twitter_account'], 'safe'],
            [['provider_name'], 'string', 'max' => 100],
            [['name', 'twitter_account'], 'string', 'max' => 150],
            [['software_categories'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'provider_name' => Yii::t('app', 'Possible FK to table providers, should be provider Id'),
            'name' => Yii::t('app', 'Name or title of the software'),
            'twitter_account' => Yii::t('app', 'Twitter account or name'),
            'software_categories' => Yii::t('app', 'Tags or categories. Usually we would create another table, one to many.'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\queries\ProductsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\queries\ProductsQuery(get_called_class());
    }
}
