<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\Console;

use app\logic\helpers\FeedHelper;
use app\logic\helpers\DateHelper;
use app\logic\rules\products\ProductsRu;

/**
 * Class ImportController
 * @package app\commands
 */
class ImportController extends Controller
{
    /** Connects to the provider source and import its products in the way defined
     * in the mapping between provider format and product GetApp format.
     * @param string $provider
     * @param string $source
     * @return int
     */
    public function actionIndex($provider, $source)
    {
        $this->stdout('**** '.DateHelper::nowSQL(true).' START import on  '.$provider. " from $source \n ");

        $feed = new FeedHelper();
        $feed->parseSource($source);
        $sourceFile = $feed->getFile();

        /** @var ProductsRu $productsRules */
        $productsRules = \Yii::$container->get('Rules\Products', [\Yii::$container, new Console()]);
        if (!$productsRules->importFromProvider($provider, $sourceFile) ) {
            $this->stdout(''.DateHelper::nowSQL(true).'*** END with errors import on '.$provider. " from $source : \n ".
            $productsRules->getErrorMessage());
            return self::EXIT_CODE_ERROR;
        }

        $this->stdout(''.DateHelper::nowSQL(true).'*** END successful import on '.$provider. " from $source \n ");
        return self::EXIT_CODE_NORMAL;
    }
}
