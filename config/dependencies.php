<?php
/************ REGISTRATION OF DEPENDENCIES, mainly for RULES LOGIC classes ************************************/

// Yii dependencies ones--------------------------

// Registration of rules--------------------------
\Yii::$container->set('Rules\Products', 'app\logic\rules\products\ProductsRu');
\Yii::$container->set('Rules\CapterraProvRu', 'app\logic\rules\providers\CapterraProvRu');
\Yii::$container->set('Rules\SoftwareAdviceProvRu', 'app\logic\rules\providers\SoftwareAdviceProvRu');


// Registration of Entities-----------------------
\Yii::$container->set('Entities\Product', 'app\logic\entities\Product');
\Yii::$container->set('Entities\CapterraItem', 'app\logic\entities\providers\CapterraItem');
\Yii::$container->set('Entities\SoftwareAdviceItem', 'app\logic\entities\providers\SoftwareAdviceItem');