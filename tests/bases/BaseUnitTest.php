<?php

namespace tests\bases;

use yii\di\Container;

/**
 * Class BaseUnitTest
 * @package tests\bases
 */
abstract class BaseUnitTest extends \PHPUnit_Framework_TestCase
{
    /** @var  Container $container */
    protected $container;

    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->container = new Container();
    }


    /**
     * @param string $className
     * @return mixed|\PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockWithoutConstructor($className)
    {
        return $this->getMockBuilder($className)->disableOriginalConstructor()->getMock();
    }
}
