<?php

defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'test');

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require_once __DIR__ . '/../config/bootstrap.php';
//require_once __DIR__ . '/../tests/fixtures/ProductsFixtures.php';
require_once __DIR__ . '/../tests/bases/BaseUnitTest.php';

//$config = yii\helpers\ArrayHelper::merge(
//    require_once __DIR__ . '/../config/db.php',
//    require_once __DIR__ . '/../config/test.php',
//    require_once __DIR__ . '/../config/test_db.php'
//);
//require(__DIR__ . '/config/bootstrap.php');

$config['id'] = 'test';
$config['basePath'] = __DIR__;

\Yii::$app = new yii\web\Application($config);
