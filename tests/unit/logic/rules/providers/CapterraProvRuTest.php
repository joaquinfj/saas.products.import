<?php

namespace tests\unit\logic\rules\providers;

use app\logic\entities\providers\CapterraItem;
use app\logic\rules\providers\CapterraProvRu;

use tests\bases\BaseUnitTest;

/**
 * Class CapterraProvRuTest
 * @covers CapterraProvRu
 * @package tests\unit\logic\rules\providers
 */
class CapterraProvRuTest extends  BaseUnitTest
{
    /** @var  CapterraProvRu $capterraRu */
    protected $capterraRu;

    protected function setUp()
    {
        $this->capterraRu = new CapterraProvRu($this->container, \Yii::getLogger());
    }

    /**
     * @test CapterraProvRu::mapItemToProduct
     * @return bool
     */
    public function testMapItemToProduct()
    {
        $capterraItem = new CapterraItem(CapterraProvRu::$providerName);

        $capterraItem->setName('Producto-de-test-Capterra');
        $prodTest = $this->capterraRu->mapItemToProduct($capterraItem);

        $this->assertEquals('Product', get_class($prodTest));
        $this->assertEquals('Producto-de-test-Capterra', $prodTest->name);

        return true;
    }

    /**
     * @test CapterraProvRu::parseSourceIntoItems
     * @return bool
     */
    public function testParseSourceIntoItems()
    {
        $sourceTest = '{
                    "products": [
                        {
                            "categories": [
                                "Categoria1",
                                "Categoria2"
                            ],
                            "twitter": "@GetApp",
                            "title": "GetApp"
                        }
                    ]
            }';
        /** @var CapterraItem|\PHPUnit_Framework_MockObject_MockObject $capterraItem */
        $capterraItem = $this->getMockBuilder('app\logic\entities\providers\CapterraItem')
            ->disableOriginalConstructor()
            ->getMock();
        $capterraItem->method('parseObjectIntoAttributes')->will($this->returnValue(true));
        $this->container->set('Entities\CapterraItem', $capterraItem);

        $ret = $this->capterraRu->parseSourceIntoItems($sourceTest);
        $this->assertEquals(1, count($ret), 'Should be only one item');
        return true;
    }

    /**
     * @test SoftwareAdviceProvRu::parseSourceIntoItems
     * @return bool
     */
    public function testFalseParseSourceIntoItems()
    {
        $sourceTest = '{
                    "products": [
                        {
                            "categories": [
                                "Categoria1",
                                "Categoria2"
                            ],
                        }
                    ]
            }';
        /** @var CapterraItem|\PHPUnit_Framework_MockObject_MockObject $softMockItem */
        $softMockItem = $this->getMockBuilder('app\logic\entities\providers\CapterraItem')
            ->disableOriginalConstructor()
            ->getMock();
        $softMockItem->method('parseObjectIntoAttributes')->will($this->returnValue(false));
        $this->container->set('Entities\CapterraItem', $softMockItem);

        $ret = $this->capterraRu->parseSourceIntoItems($sourceTest);
        $this->assertFalse($ret);

        return true;
    }
}