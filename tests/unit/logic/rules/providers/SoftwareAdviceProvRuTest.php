<?php

namespace tests\unit\logic\rules\providers;

use app\logic\entities\providers\SoftwareAdviceItem;
use app\logic\rules\providers\SoftwareAdviceProvRu;

use tests\bases\BaseUnitTest;

/**
 * Class SoftwareAdviceProvRuTest
 * @covers SoftwareAdviceProvRu
 * @package tests\unit\logic\rules\providers
 */
class SoftwareAdviceProvRuTest extends  BaseUnitTest
{
    /** @var  SoftwareAdviceProvRu $softAdviceRu */
    protected $softAdviceRu;

    protected function setUp()
    {
        $this->softAdviceRu = new SoftwareAdviceProvRu($this->container, \Yii::getLogger());
    }

    /**
     * @test SoftwareAdviceProvRu::mapItemToProduct
     * @return bool
     */
    public function testMapItemToProduct()
    {
        $softItem = new SoftwareAdviceItem(SoftwareAdviceProvRu::$providerName);

        $softItem->setTitle('Producto-de-test');
        $prodTest = $this->softAdviceRu->mapItemToProduct($softItem);

        $this->assertEquals('Product', get_class($prodTest));
        $this->assertEquals('Producto-de-test', $prodTest->name);

        return true;
    }

    /**
     * @test SoftwareAdviceProvRu::parseSourceIntoItems
     * @return bool
     */
    public function testParseSourceIntoItems()
    {
        $sourceTest = '{
                    "products": [
                        {
                            "categories": [
                                "Categoria1",
                                "Categoria2"
                            ],
                            "twitter": "@GetApp",
                            "title": "GetApp"
                        }
                    ]
            }';
        /** @var SoftwareAdviceItem|\PHPUnit_Framework_MockObject_MockObject $softMockItem */
        $softMockItem = $this->getMockBuilder('app\logic\entities\providers\SoftwareAdviceItem')
            ->disableOriginalConstructor()
            ->getMock();
        $softMockItem->method('parseObjectIntoAttributes')->will($this->returnValue(true));
        $this->container->set('Entities\SoftwareAdviceItem', $softMockItem);

        $ret = $this->softAdviceRu->parseSourceIntoItems($sourceTest);
        $this->assertEquals(1, count($ret), 'Should be only one item');
        return true;
    }

    /**
     * @test SoftwareAdviceProvRu::parseSourceIntoItems
     * @return bool
     */
    public function testFalseParseSourceIntoItems()
    {
        $sourceTest = '{
                    "products": [
                        {
                            "categories": [
                                "Categoria1",
                                "Categoria2"
                            ],
                        }
                    ]
            }';
        /** @var SoftwareAdviceItem|\PHPUnit_Framework_MockObject_MockObject $softMockItem */
        $softMockItem = $this->getMockBuilder('app\logic\entities\providers\SoftwareAdviceItem')
            ->disableOriginalConstructor()
            ->getMock();
        $softMockItem->method('parseObjectIntoAttributes')->will($this->returnValue(false));
        $this->container->set('Entities\SoftwareAdviceItem', $softMockItem);

        $ret = $this->softAdviceRu->parseSourceIntoItems($sourceTest);
        $this->assertFalse($ret);

        return true;
    }
}