<?php

namespace tests\unit\logic\rules;

use app\logic\rules\products\ProductsRu;
use app\logic\rules\providers\ProvidersRu;

use tests\bases\BaseUnitTest;

/**
 * Class ProductsRuTest
 * @covers ProductsRu
 * @package tests\unit\logic\rules
 */
class ProductsRuTest extends BaseUnitTest
{
    /** @var  ProvidersRu|\PHPUnit_Framework_MockObject_MockBuilder $providerRu */
    protected $providerRu;

    /**
     *
     */
    protected function setUp()
    {
        parent::setUp();

        $this->providerRu = $this->getMockBuilder('app\logic\rules\providers\CapterraProvRu')
            ->disableOriginalConstructor()
            ->getMock();
        $this->providerRu->method('readFeed')->will($this->returnValue(array()));
        $this->container->set('Rules\CapterraProvRu', $this->providerRu);
    }

    /**
     * @test
     * @covers ProductsRu::importFromProvider
     * @return bool
     */
    public function testImportFromProvider()
    {
        $this->assertTrue(true, 'Here we would test the main method importFromProvider');

        return true;
    }
}